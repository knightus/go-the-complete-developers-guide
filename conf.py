import sys, os
import sphinx_rtd_theme
from recommonmark.parser import CommonMarkParser

project = 'Go: The Complete Developer\'s Guide'
copyright = '2018, William Le'
author = 'William Le'


templates_path = ['_templates']

source_suffix = [
    '.rst',
    '.md'
]

source_parsers = {
    '.md': CommonMarkParser,
}

version = '1.0'
release = '1.0'

htmlhelp_basename = 'ReadTheDocsdoc'

language = 'en'

highlight_language = 'javascript'

pygments_style = 'sphinx'

master_doc = 'index'

exclude_patterns = ['node_modules/*', 'public/*']

intersphinx_mapping = {
    'sphinx': ('http://sphinx.readthedocs.io/en/latest/', None),
}

latex_documents = [
    ('index', 'go-the-complete-developers-guide-oct-2018.tex', u'Go: The Complete Developer\'s Guide',
     u'Duy Pham', 'manual'),
]

man_pages = [
    ('index', 'go-the-complete-developers-guide-oct-2018', u'Go: The Complete Developer\'s Guide',
     [u'William Le'], 1)
]

extensions = [
  'sphinx.ext.autosectionlabel',
  'sphinx.ext.autodoc',
  'sphinx.ext.intersphinx',
]

html_theme = 'sphinx_rtd_theme'

html_theme_path = [sphinx_rtd_theme.get_html_theme_path()]

scv_greatest_tag = True
scv_grm_exclude = ('README.rst', '.gitignore')
scv_show_banner = True
scv_sort = ('semver', 'time')
scv_whitelist_branches = ('master', 'develop')
